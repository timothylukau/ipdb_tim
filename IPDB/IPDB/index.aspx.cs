﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
//using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Collections.Specialized;

namespace IPDB
{
    public partial class index : System.Web.UI.Page
    {
        private string pullDate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect("/Maintenance.html");

            Get_PullDate(); 
            Check_MyOGIStatus();

            Load_DataHeader();

            Load_DataPanel();

        }

        public void Load_DataHeader()
        {
            HttpCookie IP = new HttpCookie("IP");
            IP = Request.Cookies["IP"];
            if (IP != null) //Yes, let them in!
            {
                //Commented out the list details of tracker and funding on Feb 23, 2017 - Ganesh
                //Added the Microcampuses a tag right after the CESL one. Alan June 15th 2018
                //Commented out the list details of UATravel - Matthew
                //headerDataPanel.InnerHtml =
                //       "<ul>" +
                //       "<li><a class=\"thumb\" href=\"/img/btn_Partners.png\" id=\"DS_Partners\" onmouseover=\"DS_Over('Partners');\" onmouseout=\"DS_Out('Partners');\" onclick=\"DS_Click('Partners');\"></a></li>" +
                //       "</ul>";
            }
        }

        public void Get_PullDate()
        {
            string date = string.Empty;
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                
                SqlCommand Com = new SqlCommand("Get_RefreshDate", Conn);
                StringBuilder sb = new StringBuilder();
                Conn.Open();
                Com.CommandType = CommandType.StoredProcedure;
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    sb.Append(r[0]);
                }
                Conn.Close();
                date = sb.ToString().Split()[0];
            }
            pullDate = date;
        }

        public void Load_DataPanel()
        {
            //Look for a IP cookie
            HttpCookie IP = new HttpCookie("IP");
            IP = Request.Cookies["IP"];

            //Start the stringbuilder object
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id=\"DataPanelWrapper\">");

            //------------------------------------
            //Partners DataPanel
            //------------------------------------
            sb.Append(@"<div id=""QueryBar_Partners"" class=""DataPanel_InnerBox"">");

            //Title
            sb.Append("<div class=\"DataPanel_Title\">UA Global Institutional Partners</div>");

            //Description
            sb.Append("<div class=\"DataPanel_Text\">Organizations with which the UA maintains faculty or student mobility agreements.</div>");

            //DataPanel_FormContainer
            sb.Append("<div class=\"DataPanel_FormContainer\">");

            //QueryBar:Status

            /*--Remove Status Option--*/
            //sb.Append("<span class=\"DataPanel_Label\">Status</span><select id=\"IR_Status\" onChange=\"PostModel_11()\" class=\"DataPanel_FormElement\">");
            //sb.Append("<select class=\"DataPanel_FormElement\">");
            //sb.Append("<option value=\"Active\">Active</option>)");

            //sb.Append("</select><br />");

            //QueryBar:Category
            sb.Append("<span class=\"DataPanel_Label\">Type</span><select id=\"IR_Category\" onChange=\"PostModel_11()\" class=\"DataPanel_FormElement\">");
            sb.Append("<option value=\"All\">Any Agreement</option>)");
            // Commented the old code.
            //sb.Append("<option value=\"Dual Degree\">Dual Degree</option>)");
            //sb.Append("<option value=\"IMOA\">IMOA</option>)");
            //sb.Append("<option value=\"IMOU\">Memorandum of Understanding</option>)");
            //sb.Append("<option value=\"Transfer\">Transfer Articulation Agreement</option>)");
            //sb.Append("<option value=\"ResearchColloboration\">Research Collaboration</option>)");
            //sb.Append("<option value=\"FacultyExchange\">Faculty Exchange</option>)");
            //sb.Append("<option value=\"StudentExchange\">Student Exchange</option>)");
            //sb.Append("<option value=\"StudyAbroadExchange\">Study Abroad Exchange</option>)");
            sb.Append("</select><br />");

            //QueryBar:UA Department
            sb.Append("<span class=\"DataPanel_Label\">UA Affiliate</span><select id=\"IR_Department\" onChange=\"PostModel_11()\" class=\"DataPanel_FormElement\">");
            sb.Append("<option value=\"All\">All Departments</option>)");
            sb.Append("</select><br />");

            //Close DataPanel_FormContainer
            sb.Append("</div>");

            //Legend
            //sb.Append("<div class=\"DataPanel_Legend2\"><img src=\"/img/BlueSquare.png\"> = Partnering Institution</div>");
            sb.Append("<div class=\"DataPanel_Legend\"></div>");

            //Source
            sb.Append("<div class=\"DataPanel_Text\"><span class=\"DataPanel_SourceLabel\">Source: </span>UA Global Partnership Database<br/><br/><span class=\"DataPanel_SourceLabel\">Refresh Date: </span>"+pullDate+"<br></br></div>");

            sb.Append("</div>");

            sb.Append("</div>");

            //------------------------------------
            //OUTPUT to Code Front
            //------------------------------------

            lit_DataPanel.Text = sb.ToString();

        }

        private void Check_MyOGIStatus()
        {

            lit_UABrandingBar.Text = "<header><div class=\"inner\"><a id=\"UA_OGI_Logo\" href=\"http://global.arizona.edu\" target = \"blank\"><img alt=\"UA Office of Global Initiatives\" title=\"UA Global\" src=\"/img/ogi_logo.png\" width=\"249\" height=\"38\" /></a><form id=\"header-ualogo\"><div class=\"right\"><a id=\"UA_EDU_Logo\" href=\"http://www.arizona.edu\"><img alt=\"UA Home\" title=\"University of Arizona\" src=\"/img/UA_EDU_Logo.png\" width=\"179\" height=\"32\" /></a></div></form></div></header>";

            HttpCookie IP = new HttpCookie("IP");
            IP = Request.Cookies["IP"];
            if (IP == null)
            {
                //Set a special cookie
                string Token = "";
                Token = "IP" + ForgeToken(6);
                HttpCookie SetIPCookie = new HttpCookie("IP");
                SetIPCookie["Token"] = Token;
                Response.Cookies.Add(SetIPCookie);

                string ServerToken = ForgeToken(8);

                //Write to Database
                using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
                {
                    Conn.Open();
                    SqlCommand Com = new SqlCommand("INSERT INTO UserSessions (UserId, Token,ServerToken, StartTime) SELECT @UserId, @Token, @ServerToken, GETDATE()", Conn);
                    Com.Parameters.Add(new SqlParameter("UserId", "Guest"));
                    Com.Parameters.Add(new SqlParameter("Token", Token));
                    Com.Parameters.Add(new SqlParameter("ServerToken", ServerToken));
                    Com.ExecuteNonQuery();
                }
            }

        }

        public static void WriteLog(string Event, string Token)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_WriteLog", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("Event", Event);
                Com.Parameters.AddWithValue("Token", Token);
                Com.ExecuteNonQuery();
            }
        }

        public static string ForgeToken(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        private string Get_TargetDate(int Offset)
        {
            DateTime TargetDate = DateTime.Today;
            TargetDate = TargetDate.AddDays(Offset);
            return TargetDate.ToString("MM/dd/yyyy");
        }
    }
}