﻿$(document).ready(function () {
    console.log("ready!");

});

var ActiveDataPanel = {}; //declare here for global scoping

// Declare a global window that can store the current window opened.
var prev_InfoWindow = new google.maps.InfoWindow({
    content: ""
});
function Load_DataPanel() { //Checks URL for a DataPanel parameter, sets the slider to the chosen DataSet, then calls DS_Click

    var nav = getCookie("returnNavigation");

    var DataPanel = "";
    var ReqDataPanel = url('?DataPanel');
    if (nav !== "") {
        ReqDataPanel = nav;
    }
    var DisplayIndex = 0;
    if (ReqDataPanel === "Partners") { DisplayIndex = 0; DataPanel = ReqDataPanel; }
    if (DataPanel === "") { DisplayIndex = 0; DataPanel = "Partners"; } //Sets default DataPanel to Partners Database if querystring contains anything other than one of the above valid requested DataPanels - safer this way.
    //mcThumbnailSlider.display(DisplayIndex);
    DS_Click(DataPanel);
}

function DS_Click(DataPanel) { //Loads a requested Data Panel and calls the map everytime a DataPanel button is clicked, or the when the page loads (from Load_DataPanel()).

    if (DataPanel === "Partners") {
        ActiveDataPanel.Value = "Partners";
        DS_ClearAll("Partners");
        hideQB();
        $("#QueryBar_Partners").show();
        PostModel_11();
    }
}

function hideQB() {
    $("#QueryBar_Partners").hide();
}

function DS_Over(DataPanel) { //DataPanel button MouseOver function
    //None of the following work :'(
    //document.getElementById("DS_" + DataPanel).href = "/img/btn_" + DataPanel + "_Hover.png";
    document.getElementById("DS_" + DataPanel).style.backgroundImage = "url(/img/btn_" + DataPanel + "_Hover.png)";
    //$(DataPanel).css('background-image', 'url(/img/btn_'+DataPanel+'_Hover.png);');
    //alert(document.getElementById("DS_" + DataPanel).href);
}

function DS_Out(DataPanel) { //DataPanel button MouseOut function
    if (DataPanel !== ActiveDataPanel.Value) {
        document.getElementById("DS_" + DataPanel).style.backgroundImage = "url(/img/btn_" + DataPanel + ".png)";
    }
}

function DS_ClearAll(DataPanel) {
    //document.getElementById("DS_Partners").href = "/img/btn_Partners.png";

    //document.getElementById("DS_Partners").style.backgroundImage = "url(/img/btn_Partners.png)";
    //document.getElementById("DS_" + DataPanel).style.backgroundImage = "url(/img/btn_" + DataPanel + "_Hover.png)";
}

//Initialize DateRange Pickers
$(function () {
    var cook = $.cookie("IP");
    if (cook === undefined) {
        $("#IRiskFrom").datepicker({
            defaultDate: "-1m",
            maxDate: "-1m",
            onClose: function (selectedDate) {
                $("#IRiskTo").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#IRiskTo").datepicker({
            defaultDate: "-2m",
            maxDate: "-1m",
            onClose: function (selectedDate) {
                $("#IRiskFrom").datepicker("option", "maxDate", selectedDate);
            }
        });
    }
    else {
        $("#IRiskFrom").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function (selectedDate) {
                $("#IRiskTo").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#IRiskTo").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function (selectedDate) {
                $("#IRiskFrom").datepicker("option", "maxDate", selectedDate);
            }
        });
    }
});

//-----------------------------------------------
// MODEL CALLS
//-----------------------------------------------
//Draw Institutional Partners Map
function PostModel_11() {

    $(".DataPanel_Legend").empty();
    var Token = $.cookie("IP");
    var FormData = {
        IR_Category: $('#IR_Category').val(),
        IR_Status: $('#IR_Status').val(),
        IR_Department: $('#IR_Department').val()
    }

    //Values to be checked by query filters to know which one is selected by user and given the selected attribute
    var IR_Status_Checked = $('#IR_Status').val();
    var IR_Category_Checked = $('#IR_Category').val();
    var IR_Department_Checked = $('#IR_Department').val();

    $.post('/api/IP/Model_11?' + Token + '', FormData)
        .done(function (data) {
            var GeoJSONString = JSON.parse(data.DynamicMap);
            VisLayer.setMap(null);
            VisLayer = new google.maps.Data();
            infoWindow = new google.maps.InfoWindow({
                content: ""
            });

            VisLayer.addGeoJson(GeoJSONString);
            //Close any open window only if the current window is not null.
            //Any infowindow open in other datapanels will be closed when the Partner thumbnail is clicked.
            if (prev_InfoWindow !== null)
                prev_InfoWindow.close();

            VisLayer.addListener('click', function (event) {
                infoWindow.close();
                //Close the previous info window after a click event is fired. 
                if (prev_InfoWindow !== null)
                    prev_InfoWindow.close();
                postmodel_25(event);
            });

            // Apply the Circle Icons style
            VisLayer.setStyle(function (feature) {
                var Mag = parseFloat(feature.getProperty('Mag'));
                return ({
                    icon: {
                        //path: 'M -0.35,-0.35 0.35,-0.35 0.35,0.35 -0.35,0.35 z',
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: Mag,
                        fillColor: '#AB0520',
                        fillOpacity: .50,
                        strokeWeight: 1,
                        strokeColor: '#000000'
                    }
                });
            });

            VisLayer.setMap(map);
            $.post('/api/IP/Model_73?' + Token)
                .done(function (data) {
                    $(data).each(function (index) {
                        $(".DataPanel_Legend").append("<img style='vertical-align:middle;' src='/img/Dot" + "_" + [index] + ".png'/>" + " " + data[index] + "<br>");
                    });
                })
                .fail(function (jqXHR, textStatus, err) {
                    $('map-canvas').innerHTML = 'Error: ' + err;
                });

        })

        .fail(function (jqXHR, textStatus, err) {
            $('map-canvas').innerHTML = 'Error: ' + err;
        });


    //Status Query Filter
    $.post('/api/IP/Model_67?' + '', FormData)
        .done(function (data) {
            document.getElementById("IR_Status").innerHTML = "<option value=\"All\">All</option>";

            $(data).each(function (index) {
                if (data[index] === IR_Status_Checked) {
                    document.getElementById("IR_Status").innerHTML += "<option value= \"" + data[index] + "\" selected>" + data[index] + "</option>";
                }
                else {
                    document.getElementById("IR_Status").innerHTML += "<option value= \"" + data[index] + "\">" + data[index] + "</option>";
                }
            });
        });

    //Type Query Filter
    $.post('/api/IP/Model_68?' + '', FormData)
        .done(function (data) {
            document.getElementById("IR_Category").innerHTML = "<option value=\"All\">All</option>";

            $(data).each(function (index) {
                if (data[index] === IR_Category_Checked) {
                    document.getElementById("IR_Category").innerHTML += "<option value= \"" + data[index] + "\" selected>" + data[index] + "</option>";
                }
                else {
                    document.getElementById("IR_Category").innerHTML += "<option value= \"" + data[index] + "\">" + data[index] + "</option>";
                }
            });
        });

    //College Query Filter
    $.post('/api/IP/Model_69?' + '', FormData)
        .done(function (data) {
            document.getElementById("IR_Department").innerHTML = "<option value=\"All\">All</option>";

            $(data).each(function (index) {
                if (data[index] === IR_Department_Checked) {
                    document.getElementById("IR_Department").innerHTML += "<option value= \"" + data[index] + "\" selected>" + data[index] + "</option>";
                }
                else {
                    document.getElementById("IR_Department").innerHTML += "<option value= \"" + data[index] + "\">" + data[index] + "</option>";
                }
            });
        });

}

function postmodel_25(event) {
    var Token = $.cookie("IP");
    var ThisID = event.feature.getProperty('ID');
    var Lat = event.feature.getProperty('Lat');
    var Lon = event.feature.getProperty('Lon');
    var LatLon = Lat + "^" + Lon;
    var anchor = new google.maps.MVCObject();
    anchor.set("position", event.latLng);
    var infoWindow = new google.maps.InfoWindow({
        content: ""
    });
    $.post('/api/IP/Model_25?' + Token + '&Id=' + ThisID + '&LatLon=' + LatLon) //fetch summary data
        .done(function (data) {
            var NumOpps = data[0];
            var Country = data[1];
            var City = data[2];
            //Convert to English
            var NumOppsLabel = NumOpps + " Partners";
            if (NumOpps === 1) { NumOppsLabel = "1 partner"; }
            var content = '<div style="text-center" id="content">' +
                '<div class="InfoWindow_Country">' + Country + ',' + City + '</div>' +
                '<div class="InfoWindow_Text">' + NumOppsLabel + '</div><br />';
            $.post('/api/IP/Model_74?' + Token) //fetch detail data
                .done(function (data1) {
                    var PartnerInstitution = "";
                    var Department = "";
                    var Discipline = "";
                    var UAFacultySponsorEmail = "";
                    var UAFacultySponsor = "";
                    var ResearchCollaboration = "";
                    var FacultyExchange = "";
                    var StudentExchange = "";
                    var GraduateExchange = "";
                    var UndergradExchange = "";
                    var InceptionDate = "";
                    var PartnerURL = "";
                    var PartnerStatus = "";
                    var exchangeString = "";
                    var exchangeList = []; //This is to store all the exchange options in a list. To add sense to the string that will displayed.

                    var ResultArray = data1.split("!#!");
                    for (i = 0; i < ResultArray.length; i++) {
                        var InnerResultArray = ResultArray[i].split("^");
                        PartnerInstitution = InnerResultArray[0];
                        Department = InnerResultArray[1];
                        Discipline = InnerResultArray[2];
                        UAFacultySponsorEmail = InnerResultArray[3];
                        ResearchCollaboration = InnerResultArray[4];
                        FacultyExchange = InnerResultArray[5];
                        StudentExchange = InnerResultArray[6];
                        GraduateExchange = InnerResultArray[7];
                        UndergradExchange = InnerResultArray[8];
                        UAFacultySponsor = InnerResultArray[9];
                        InceptionDate = InnerResultArray[10];
                        PartnerURL = InnerResultArray[11];
                        PartnerStatus = InnerResultArray[12];
                        exchangeList = [];
                        exchangeString = "";

                        if (InceptionDate !== '') {// Go for Status only when incpetion date is not empty.
                            if (PartnerStatus !== '') {
                                if (PartnerStatus.toLowerCase() === 'Active') {//Display based on the status.
                                    InceptionDate = '<div align="right"><font color="red"><strong>' + PartnerStatus + ' since </strong>' + InceptionDate + '</font></div>';
                                }
                                else if (PartnerStatus.toLowerCase() === 'In preparation') {
                                    InceptionDate = '<div align="right"><font color="red"><strong>' + PartnerStatus + ' since </strong>' + InceptionDate + '</font></div>';
                                }
                                else if (PartnerStatus.toLowerCase() === 'Expired') {
                                    InceptionDate = '<div align="right"><font color="red"><strong>' + PartnerStatus + ' on </strong>' + InceptionDate + '</font></div>';
                                }
                            }
                        }

                        if (Department !== '') {
                            if (Department === Discipline) { //If the department and Discipline are same then only department is displayed. 
                                Department = '<div class="InfoWindow_spacing"><strong>Department/College: </strong>' + Department + '.</div>';
                            }
                            else {
                                if (Discipline === '') {//If discipline is empty then just use Department.
                                    Department = '<div class="InfoWindow_spacing"><strong>Department/College: </strong>' + Department + '.</div>';
                                }
                                else {
                                    Department = '<div class="InfoWindow_spacing"><strong>Department/College: </strong>' + Department + ', ' + Discipline + '.</div>';
                                }
                            }
                        }

                        if (UAFacultySponsor !== '') {
                            if (UAFacultySponsorEmail.includes('no longer')) {//If the email no longer exists then don't display it to the users.
                                UAFacultySponsor = '<div class="InfoWindow_spacing"><strong>UA Faculty Sponsor: </strong>' + UAFacultySponsor + '.</div>';
                            }
                            else {
                                UAFacultySponsor = '<div class="InfoWindow_spacing"><strong>UA Faculty Sponsor: </strong>' + UAFacultySponsor + ', ' + UAFacultySponsorEmail + '.</div>';
                            }
                        }

                        if (ResearchCollaboration !== '') { if (ResearchCollaboration.toLowerCase() === 'yes') { exchangeList.push('Research Collaboration'); } }//Add the exchange options into a list
                        if (FacultyExchange !== '') { if (FacultyExchange.toLowerCase() === 'yes') { exchangeList.push('Faculty Exchange'); } }
                        if (StudentExchange !== '') { if (StudentExchange.toLowerCase() === 'yes') { exchangeList.push('Student Exchange'); } }
                        if (GraduateExchange !== '') { if (GraduateExchange.toLowerCase() === 'yes') { exchangeList.push('Graduate Exchange'); } }
                        if (UndergradExchange !== '') { if (UndergradExchange.toLowerCase() === 'yes') { exchangeList.push('Undergrad Exchange'); } }

                        if (exchangeList.length === 1) { //If there is only one option then we don't have to use an 'and'.
                            exchangeString += exchangeList[0] + '.';
                        }
                        else if (exchangeList.length === 2) {
                            exchangeString += exchangeList[0] + ' and  ' + exchangeList[1] + '.';//Append an 'and' for the last exchange option.
                        }
                        else if (exchangeList.length > 1) {
                            for (var j = 0; j < exchangeList.length; j++) {
                                if (j === (exchangeList.length) - 1) {
                                    exchangeString += 'and ' + exchangeList[j] + '.';//Append an 'and' for the last exchange option.
                                    break;
                                }
                                else {
                                    exchangeString += exchangeList[j] + ', ';
                                }
                            }
                        }
                        if (PartnerURL !== '') { PartnerURL = '<div class="InfoWindow_spacing">' + '<a href=\'' + PartnerURL + '\' target=\'_blank\'>' + PartnerURL + '</a></div>'; }

                        content = content + '<div class="InfoWindow_InnerDiv4">' + '<div class="InfoWindow_SubTitle">' + PartnerInstitution + '</div>' + InceptionDate + Department + UAFacultySponsor + exchangeString + PartnerURL + '</div>';
                    }
                    content = content + '</div>';
                    infoWindow.setContent(content);
                    //Save the infowindow content in the globally declared infowindow(prev_InfoWindow)
                    //Display the content using the globally declared window. We can resolve the 'Close previous info window problem' with this declaration.
                    //Helps in keeping track of any infowindow that is open. 
                    prev_InfoWindow = infoWindow;
                    prev_InfoWindow.open(map, anchor);
                })
                .fail(function (jqXHR, textStatus, err) {
                    $('map-canvas').innerHTML = 'Error: ' + err;
                });
        })
        .fail(function (jqXHR, textStatus, err) {
            $('map-canvas').innerHTML = 'Error: ' + err;
        });
}

function redirectToLogin() {
    document.cookie = "returnNavigation=" + ActiveDataPanel.Value + ";";
    window.location.href = "login.aspx";
}


function redirectToLogout() {
    delete_cookie("IP");
    //window.location.href = "https://webauth.arizona.edu/webauth/logout?logout_href=" + window.location.href + "&logout_text=Return%20To%20MapsGlobal";


    //window.location.href = "https://webauth.arizona.edu/webauth/logout?logout_href=https://maps.global.arizona.edu&logout_text=Return%20To%20MapsGlobal";
    //window.location.href = "https://webauth.arizona.edu/webauth/logout?logout_href=https://tikal2.global.arizona.edu&logout_text=Return%20To%20MapsGlobal";
    //window.location.href = "https://webauth.arizona.edu/webauth/logout?logout_href=http://localhost:51632&logout_text=Return%20To%20MapsGlobal";
}

//Thank you StackOverflow~
function delete_cookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}