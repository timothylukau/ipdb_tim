﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="IPDB.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!-- Author: Ash Black
                 Data Scientist, Director of Tech.Global student engagement program
                 Office of Global Initiatives
                 (520) 395-6523
                 888 N. Euclid Ave, Tucson Arizona 85718

                 Maps.Global is a Google Maps API, MVC based platform developed at the University of Arizona for the visualization of UA data of international scope.

                It is maintained and continously developed for the University via the Tech.Global student engagement program providing not only rich visualization capabilities but also serving as a starting point for student mentoring as it rapidly introduces new programmers to the advanced concepts of MVC architecture - HTML5/CSS, C#, Google Maps/APIs, and T-SQL based development.

                (c) 2015 - 2018 Arizona Board of Regents.

        -->

    <meta charset="utf-8" />
    <title>Maps.Global - University of Arizona International Data Portal</title>

    <!-- Favicon -->
    <link type="image/x-icon" rel="shortcut icon" href="/img/favicon.ico" />

    <!-- Google Maps API Call with OGI token -->
    <!--Added googleapis initialization call before the java script files are initialized. This Fixed the google maps reference error(because prev_InfoWindow was declared globally in blackdog.js) and also fixed date picker initialization bug. -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyDiLYKLZX9meHI_n6dX2nd_W3cDSHyD_SA"></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

    <!-- Javascript Resources -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="js/index.js"></script>
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/thumbnail-slider.js" type="text/javascript"></script>
    <script src="/js/url.min.js" type="text/javascript"></script>

    <!-- CSS Resources -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link href="css/index.css" rel="stylesheet" />
    <link href="/css/thumbs2.css" rel="stylesheet" />
    <link href="/css/thumbnail-slider.css" rel="stylesheet" />

    <!-- Google Map Initialization -->
    <script type="text/javascript">

        var map;

        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(15, 0),
                zoom: 3, //0-21
                minZoom: 3,
                //OGI Custom Map Style
                styles: [{ "featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "weight": "0.60" }] }, { "featureType": "administrative.country", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.province", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.neighborhood", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "geometry.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text.fill", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.natural.landcover", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "landscape.natural.terrain", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.arterial", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.local", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "labels", "stylers": [{ "visibility": "off" }] }],
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                scaleControl: true,  // fixed to BOTTOM_RIGHT
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                }
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

            //Initialize VisLayer
            VisLayer = new google.maps.Data();

        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>

</head>

<body onload="Load_DataPanel();">
    <!-- Renders a map visualization on initial load, the default being "Partners" see BlackDog.js -->

    <!-- Implement an hour glass model for use with the slower drawing UA Global Alumni map (loads the gif while data is being fetched and dims  the map (Ganesh Raikhelkar October 2016) -->
    <div id="loading">
        <img id="loading_box" src="/img/Modalhour58.gif" alt="Hour glass"></div>

    <!-- UA Branding Bar -->
    <asp:Literal ID="lit_UABrandingBar" runat="server"></asp:Literal>

    <!-- DataPanel.  This is the main navigational element.  It looks like a classic "Map Legend" which floats (and can be moved) upon the map but it actually is a dynamic and extensible component which is the home to all controls related to exploring individual data sets and filtering upon their unique properties.  This relatively complex component is what makes Maps.Global appear to be such a 'simple' application.  All functionality is contained within this component, and the results of its use are drawn to the map.-->
    <div id="DataPanel">

        <!-- DataPanel Heading -->
        <%--<div class="DataPanel_Header"><b>MAPS.</b>GLOBAL</div>--%>

<%--        <!-- The DataPanel Slider (this draws the slider with the PUBLIC data panels available) -->
        <div id="thumbnail-slider">
            <div class="inner" runat="server" id="headerDataPanel">
                <ul>
                    <li>
                        <a class="thumb" href="/img/btn_Partners.png" id="DS_Partners" onmouseover="DS_Over('Partners');" onmouseout="DS_Out('Partners');" onclick="DS_Click('Partners');"></a>
                    </li>
                    
                </ul>
            </div>
        </div>--%>

        <!-- DataPanels.  This draws the data panels from code behind -->
        <asp:Literal ID="lit_DataPanel" runat="server"></asp:Literal>

    </div>

    <!-- The Google Map -->
    <div id="map-canvas" style="z-index: 1;" />

    <script type="text/javascript">

	    <!-- Call Document Ready -->
    $(document).ready(function () {
        $("#DataPanel").draggable(); //Make the DataPanel draggable
        VisLayer = new google.maps.Data(); //Initialize the VisLayer object
    });

    //Goolge Analytics
    (function (i, s, o, g, r, a, m) { i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date(); a = s.createElement(o), m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-31635729-6', 'auto');
    ga('send', 'pageview');
    </script>



</body>
</html>
