﻿//using GeoJSON;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;

namespace IPDB.Controllers
{
    public class IPController : ApiController
    {//----------------------------------------------------
        // TOKEN AUTHORIZATION - Controller functions to be run non-anonymously require token validation to ensure that the function can only be called by a browser associated with a properly logged in user.
        //----------------------------------------------------
        public Boolean GoodToken(string Token)
        {
            Boolean IsGood = false;
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("SELECT Token FROM UserSessions WHERE Token = @Token", Conn);
                Com.Parameters.Add(new SqlParameter("Token", Token));
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    IsGood = true;
                }
            }
            return IsGood;
        }

        //----------------------------------------------------
        // CONTROLLERS
        //----------------------------------------------------

        //Draw Institutional Partners Map
        [HttpPost]
        public IHttpActionResult Model_11(string Token, Model_11 FormData)
        {
            //Read the form
            string IR_Category = FormData.IR_Category.ToString();

            /*--Forcing Status to be Active--*/
            //string IR_Status = FormData.IR_Status.ToString();
            string IR_Status = "Active";
            string IR_Department = FormData.IR_Department.ToString();

            //Construct the SubQuery
            StringBuilder SubQuery = new StringBuilder();
            SubQuery.Append("");

            //Start the string
            StringBuilder sb = new StringBuilder();
            sb.Append("{ \"type\": \"FeatureCollection\", \"features\": [ ");

            //Read from database
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDogWeightedMap_Partners", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("Token", Token);
                Com.Parameters.AddWithValue("IR_Status", IR_Status);
                Com.Parameters.AddWithValue("IR_Department", IR_Department);
                Com.Parameters.AddWithValue("IR_Category", IR_Category);
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string Id = r[0].ToString();
                    string Lat = r[1].ToString();
                    string Lon = r[2].ToString();
                    string Mag = r[3].ToString();
                    string ZIndex = r[4].ToString();
                    sb.Append(" { \"type\": \"Feature\", \"geometry\": {\"type\": \"Point\", \"coordinates\": [" + Lon + ", " + Lat + "]}, \"properties\": {\"ID\": " + Id + ", \"Mag\": " + Mag + ", \"ZIndex\": " + ZIndex + ", \"Lat\": " + Lat + ", \"Lon\": " + Lon + "} },");
                }
            }

            //Close up
            sb.Append("]}");
            string FinalString = sb.ToString();
            FinalString = FinalString.Replace(",]}", "]}"); //remove that pesky comma

            //Return the Dynamic Map
            BlackDog_VisLayer[] thisMap = new BlackDog_VisLayer[1];
            thisMap[0] = new BlackDog_VisLayer();
            thisMap[0].DynamicMap = FinalString;
            var ResultSet = thisMap.FirstOrDefault();
            return Ok(ResultSet);
        }

        // Draw the weighted maps for International partners.
        [HttpPost]
        public IHttpActionResult Model_73(string Token)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDogReturnTopNumRecords", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("Token", Token);
                StringBuilder sb = new StringBuilder();
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    if (r[0].ToString() != "") //This really should not be necessary, an empty result set should not pass the above while r.Read() test.  Something is wrong with the sp_BlackDogReturnTopNumRecords procedure but I don't know what it is yet, this if condition is a workaround - ash 11/4/2015.
                    {
                        int TopNumberRecords = Int32.Parse(r[0].ToString());
                        if (TopNumberRecords < 5)
                        {
                            if (TopNumberRecords < 4)
                            {
                                if (TopNumberRecords < 3)
                                {
                                    if (TopNumberRecords < 2)
                                    {
                                        sb.Append(TopNumberRecords + " Institution");
                                    }
                                    else
                                    {
                                        sb.Append(TopNumberRecords + " Institutions");
                                        sb.Append("^");
                                        sb.Append(TopNumberRecords - 1 + " Institution");
                                    }
                                }
                                else
                                {
                                    sb.Append(TopNumberRecords + " Institutions");
                                    sb.Append("^");
                                    sb.Append(TopNumberRecords - 1 + " Institutions");
                                    sb.Append("^");
                                    sb.Append(TopNumberRecords - 2 + " Institution");
                                }
                            }
                            else
                            {
                                sb.Append(TopNumberRecords + " Institutions");
                                sb.Append("^");
                                sb.Append(TopNumberRecords - 1 + " Institutions");
                                sb.Append("^");
                                sb.Append(TopNumberRecords - 2 + " Institutions");
                                sb.Append("^");
                                sb.Append(TopNumberRecords - 3 + " Institution");
                            }
                        }
                        else
                        {
                            int Division = TopNumberRecords / 4;
                            int SecondDivision = Division * 2;
                            int ThirdDivision = SecondDivision + Division;
                            sb.Append(ThirdDivision + " - " + TopNumberRecords + " Institutions");
                            sb.Append("^");
                            sb.Append(SecondDivision + " - " + ThirdDivision + " Institutions");
                            sb.Append("^");
                            sb.Append(Division + " - " + SecondDivision + " Institutions");
                            sb.Append("^");
                            sb.Append(0 + " - " + Division + " Institutions");
                        }
                    }
                }

                string returnString = sb.ToString();
                return Ok(returnString.Split(new string[] { "^" }, StringSplitOptions.None));

            }
        }

        //Partners InfoWindow Summary Data
        [HttpPost]
        public IHttpActionResult Model_25(string Token, string LatLon, string Id)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDog_InfoWindow_Partners", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("Token", Token);
                Com.Parameters.AddWithValue("Id", Id);
                Com.Parameters.AddWithValue("LatLon", LatLon);
                StringBuilder sb = new StringBuilder();
                SqlDataReader reader;
                reader = Com.ExecuteReader();
                while (reader.Read())
                {
                    sb.Append(reader[0] + "^");
                    sb.Append(reader[1] + "^ ");
                    sb.Append(reader[2]);
                }

                string returnString = sb.ToString();
                return Ok(returnString.Split(new string[] { "^" }, StringSplitOptions.None));
            }
        }


        // InfoWindow Details of each partner.
        [HttpPost]
        public IHttpActionResult Model_74(string Token)
        {
            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDog_InfoWindow_Partners_Detail", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("Token", Token);
                StringBuilder sb = new StringBuilder();
                SqlDataReader reader;
                reader = Com.ExecuteReader();
                while (reader.Read())
                {
                    sb.Append(reader[0] + "^");
                    sb.Append(reader[1] + "^");
                    sb.Append(reader[2] + "^");
                    sb.Append(reader[3] + "^");
                    sb.Append(reader[4] + "^");
                    sb.Append(reader[5] + "^");
                    sb.Append(reader[6] + "^");
                    sb.Append(reader[7] + "^");
                    sb.Append(reader[8] + "^");
                    sb.Append(reader[9] + "^");
                    sb.Append(reader[10] + "^");
                    sb.Append(reader[11] + "^");
                    sb.Append(reader[12] + "!#!");
                }

                string returnString = sb.ToString();

                //Remove that pesky !#!
                returnString = returnString.Remove(returnString.Length - 3);
                return Ok(returnString);
            }
        }

        //Opportunity Type Query Filter for Opportunity Tracker

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [HttpPost]
        public IHttpActionResult Model_67(Model_11 FormData)
        {
            string IR_Category = FormData.IR_Category.ToString();
            string IR_Department = FormData.IR_Department.ToString();

            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDog_QueryFilter_Partners", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("IR_Category", IR_Category);
                Com.Parameters.AddWithValue("IR_Department", IR_Department);
                StringBuilder sb = new StringBuilder();
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string val = r[0].ToString();
                    sb.Append(val);
                    sb.Append("^");
                }

                sb.Append("!#!");
                string returnString = sb.ToString();
                returnString = returnString.Replace("^!#!", "");
                return Ok(returnString.Split(new string[] { "^" }, StringSplitOptions.None));
            }
        }

        //Opportunity Type Query Filter for Opportunity Tracker

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [HttpPost]
        public IHttpActionResult Model_68(Model_11 FormData)
        {
            /*--Forcing Status to be Active--*/
            //string IR_Status = FormData.IR_Status.ToString();
            string IR_Status = "Active";
            string IR_Department = FormData.IR_Department.ToString();

            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDog_QueryFilter_Partners2", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("IR_Status", IR_Status);
                Com.Parameters.AddWithValue("IR_Department", IR_Department);
                StringBuilder sb = new StringBuilder();
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string val = r[0].ToString();
                    sb.Append(val);
                    sb.Append("^");
                }

                sb.Append("!#!");
                string returnString = sb.ToString();
                returnString = returnString.Replace("^!#!", "");
                return Ok(returnString.Split(new string[] { "^" }, StringSplitOptions.None));
            }
        }

        //Opportunity Type Query Filter for Opportunity Tracker

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [HttpPost]
        public IHttpActionResult Model_69(Model_11 FormData)
        {
            /*--Forcing Status to be Active--*/
            //string IR_Status = FormData.IR_Status.ToString();
            string IR_Status = "Active";
            string IR_Category = FormData.IR_Category.ToString();

            using (SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["DBConn"]))
            {
                Conn.Open();
                SqlCommand Com = new SqlCommand("sp_BlackDog_QueryFilter_Partners3", Conn);
                Com.CommandType = CommandType.StoredProcedure;
                Com.Parameters.AddWithValue("IR_Status", IR_Status);
                Com.Parameters.AddWithValue("IR_Category", IR_Category);
                StringBuilder sb = new StringBuilder();
                SqlDataReader r = Com.ExecuteReader();
                while (r.Read())
                {
                    string val = r[0].ToString();
                    sb.Append(val);
                    sb.Append("^");
                }

                sb.Append("!#!");
                string returnString = sb.ToString();
                returnString = returnString.Replace("^!#!", "");
                return Ok(returnString.Split(new string[] { "^" }, StringSplitOptions.None));
            }
        }

        //----------------------------------------------------
        // FUNCTIONS
        //----------------------------------------------------
        public string ScrubForSQL(string s)
        {
            if (s != null)
            {
                s = s.Replace("\t", "    ");
                s = s.Replace("'", "''");
            }
            return s;
        }

        public string ScrubNullString(string s)
        {
            if (s == null)
            {
                s = "";
            }
            return s;
        }

    }

    //----------------------------------------------------
    // DATA MODELS
    //----------------------------------------------------
    public class BlackDog_VisLayer
    {
        public string DynamicMap { get; set; }
    }


    public class Model_11
    {
        public string IR_Category { get; set; }
        public string IR_Status { get; set; }
        public string IR_Date { get; set; }
        public string IR_Department { get; set; }
    }


}